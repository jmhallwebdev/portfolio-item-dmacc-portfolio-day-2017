$(document).ready(function(){
	initStars();
})

// initialize stars
function initStars() {

	$('#examples').jstars({
		image_path: '../imgs'
	});

	$('#example-blue').jstars({
		image_path: '../imgs',
		style: 'blue',
    frequency: 15
	});

	$('#example-yellow').jstars({
		image_path: '../imgs',
		style: 'yellow',
		frequency: 19
	});

	$('#example-green').jstars({
		image_path: '../imgs',
		style: 'green'
	});

	$('#example-red').jstars({
		image_path: '../imgs',
		style: 'red',
		frequency: 5
	});

	$('#example-rand').jstars({
		image_path: '../imgs',
		style: 'rand'
	});
   
  $('#custom2').jstars({
		image_path: '../imgs',
    image: 'jstar-modern.png',
    style: 'rand',
    width: 27,
    height: 27,
    style_map: {
      white: 0,
      blue: -27,
      green: -54,
      red: -81,
      yellow: -108
    },
    delay: 300
	});  
}
