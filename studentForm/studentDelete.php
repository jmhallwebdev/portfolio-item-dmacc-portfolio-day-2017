<?php
	$deleteRecId = $_GET['recId'];	//Pull the presenter_id from the GET parameter
	
	include 'connection.php';		//connects to the database
	
	$sql = "DELETE FROM student_db WHERE student_ID = ?";
	//echo "<p>The SQL Command: $sql </p>";     //testing
	
	$query = $conn->prepare($sql);	//prepare the statement
	
	$query->bind_param("i",$deleteRecId);	//bind the parameter to the statement
	
	if ( $query->execute() )			//process the query
	{
		$message =  "<h1>Your record has been successfully deleted.</h1>";
		$message .= "<h3><a href='studentList.php'>View</a> student records.</h3>";	
	}
	else
	{
		$message = "<h1>You have encountered a problem with your delete.</h1>";
		$message .= "<h2 style='color:red'>" . mysqli_error($conn) . "</h2>";
	}
	$query->close();
	$conn->close();	//close the database connection

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Student Form</title>

<link rel="stylesheet" type="text/css" href="studentForm.css">
</head>
<body>
  <div>
  <img src="images/logo.png" height="200px" width="200px" class="center">
</head>

<body>

<h2>
	<?php echo $message; ?>
</h2>

</body>
</html>

